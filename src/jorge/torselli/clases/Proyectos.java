package jorge.torselli.clases;

/**
 * Created by repre on 04/07/2017.
 */
public class Proyectos
{
    private String nombreProyecto;
    private float precioProyecto;

    public Proyectos() {
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public float getPrecioProyecto() {
        return precioProyecto;
    }

    public void setPrecioProyecto(float precioProyecto) {
        this.precioProyecto = precioProyecto;
    }
}
