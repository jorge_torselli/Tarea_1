package jorge.torselli.clases;

import java.util.InputMismatchException;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * Created by repre on 05/07/2017.
 */
public class Principal
{
    public static void main(String[] args)
    {
        Empresa empresa = null;
        Scanner s = new Scanner(System.in);
        boolean salida = false;
        int opcion;

        while (!salida)
        {
            System.out.println("1. Ingresar el nombre de la empresa");
            System.out.println("2. Ingresar clientes nuevos");
            System.out.println("3. Listar clientes nuevos");
            System.out.println("4. Salida");

            try
            {
                System.out.println("Selecione una opción:");
                opcion = s.nextInt();

                switch (opcion)
                {
                    case 1:
                        System.out.println("Ingrese el nombre de la empresa:");
                        empresa = new Empresa();
                        empresa.setNombreEmpresa(s.next());
                        break;
                    case 2:
                        System.out.println("Ingrese los datos para el nuevo cliente:");
                        Clientes cliente = new Clientes();
                        System.out.println("Nombre:");
                        cliente.setNombre(s.next());;
                        System.out.println("Direccion:");
                        cliente.setDireccion(s.next());
                        empresa.nuevoCliente(cliente);
                        break;
                    case 3:
                        System.out.println("Listado de cleintes ingresados");
                        for (Clientes cl:empresa.getListadoClientes())
                        {
                            System.out.println(cl.getNombre() + " --- " + cl.getDireccion() + "\n");
                        }
                        break;
                    case 4:
                        salida = true;
                        break;
                    default:
                        System.out.println("Seleccionó una opción que no esta dentro del menú");

                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Solo números...");
                s.next();
            }
        }


    }
}
