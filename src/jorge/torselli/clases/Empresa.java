package jorge.torselli.clases;

import java.util.List;

/**
 * Created by repre on 04/07/2017.
 */
public class Empresa
{
    private String nombreEmpresa;
    private List<Clientes> listadoClientes;

    public Empresa() {
    }

    public Empresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public List<Clientes> getListadoClientes() {
        return listadoClientes;
    }

    public void nuevoCliente(Clientes cl)
    {
        listadoClientes.add(cl);
    }

}
