package jorge.torselli.clases;

import java.util.List;

/**
 * Created by repre on 04/07/2017.
 */
public class Clientes
{
    private String nombre;
    private String direccion;
    private List<Proyectos> listaProyectos;

    public Clientes() {
    }

    public Clientes(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    //Los metodos y funcionalidad para la clase proyectos
    public List<Proyectos> getListaProyectos(){
        return listaProyectos;
    }

    //Para agregar un nuevo proyecto a un cliente
    public void nuevoProyecto(Proyectos pro)
    {
        listaProyectos.add(pro);
    }

    //Para obtener el total de lo invertido por un cliente
    public float totalVentaCliente()
    {
        float tot = 0;
        for(Proyectos p:listaProyectos)
        {
            tot += p.getPrecioProyecto();
        }
        return tot;
    }


}
